import java.io.*;

class CompilationEngine {
    public String fileName;
    //private FileWriter fw;

    private VMWriter vmw;
    private SymbolTable st;
    // className is used for several purposes.
    private String className;

    // this is a special variable to tell if we are in the 'if' staetment, because
    // we have to test whether 'if' is followed by an else, therefore we have to
    // figure out a way to "skip" an advance() call if in the 'if' statement
    private boolean inIf;

    // the following strings are used to put entries into the symboltable
    private String name;
    private String kind;
    private String type;
    private int    index;   

    // tell if it is a constructor or method
    private boolean isConstructor;

    // use to match types on left and right or letStatemetn
    private String typeLeft;
    private String typeRight;
    //This variable determines if we should write to .vm file
    public boolean hasErrors;


    private boolean isVoid;

    //use to label the flow counters
    private int flowCounter;
    private void error(String s){
        System.out.println(s);
        hasErrors = true;
    }
    public CompilationEngine (String file) {
        vmw = new VMWriter(file);
        st = new SymbolTable();
        fileName = file.substring(file.indexOf("/") + 1, file.lastIndexOf("."));
        name = "";
        kind = "";
        type = "";
        index = 0;

        hasErrors = false;

        flowCounter = 0;
    }


    private void resetAttr() {
        name = "";
        kind = "";
        type = "";
        index = 0;
    }
    public void close() {
        vmw.close();
    }
    // This method is use to match simple symbols like { or so.
    public void match(JackTokenizer jk, String s) throws IOException {
        if(!jk.token.equals(s)) {
            throw new IllegalStateException("expecting a symbol: " + s + ", while we have: " + jk.token );
        } else {
            //            fw.write("<symbol>" + s + "</symbol>\n");
        }
    }

    public void compileClass (JackTokenizer jk) throws IOException {
        if (!jk.token.equals("class")) {
            //            System.out.println(jk.token);
            throw new IllegalStateException("expecting class while we have: \"" + jk.token + "\"");
        }
        // move on
        jk.advance();

        // parse the identifier of the class
        compileIdentifier(jk);
        className = jk.token;

        if (!className.equals(fileName)) {
            throw new IllegalStateException("file name: \"" + fileName + "\" and class name: \"" + className + "\" must be the same");
        }
        vmw.writeComment("className: " + vmw.getName());

        jk.advance();

        // parse the { block
        match(jk, "{");
        jk.advance();

        for(;!jk.token.equals("}"); jk.advance()){
            if(jk.token.equals("field") || jk.token.equals("static")) {

                compileVarDec(jk);

            } else if (jk.token.equals("constructor")) {

                compileMethod(jk);

            } else if (jk.token.equals("method")) {

                compileMethod(jk);
            }

        }

        match(jk, "}");

        //fw.write("</class>\n");
        }

    public void compileIdentifier(JackTokenizer jk) throws IOException {
        if (!jk.tokenType.equals("IDENTIFIER")){
            throw new IllegalStateException("expecting identifier");
        } else {
            //fw.write("<identifier>" + jk.token + "</identifier>\n");
        }
    }

    public void compileKeyword(JackTokenizer jk) throws IOException {
        if (!jk.tokenType.equals("KEYWORD")) {
            throw new IllegalStateException("expecting keyword");
        } else {
            //fw.write("<keyword>" + jk.token + "</keyword>\n");
        }
    }

    public void compileType(JackTokenizer jk) throws IOException {
        if(JackCompiler.keyword.contains(jk.token)){
            compileKeyword(jk);
        }  else {
            // this maybe a class type variable
            compileIdentifier(jk);
        }

    }

    public void compileVarDec(JackTokenizer jk) throws IOException {

        //record kind information
        kind = jk.token;
        compileKeyword(jk);
        jk.advance();

        compileVarList(jk);

        match(jk, ";");
        //fw.write("</classVarDec>\n");

    }

    public void compileArgList(JackTokenizer jk, String methodID) throws IOException {
        //fw.write("<parameterList>\n");
        resetAttr();
        kind = "argument";

        //remember if we are defining a method, you should add the class as the
        //first argument, the following code has no real purpose, but a reminder
        //of this fact.
        if(!isConstructor) {
            type = "method";
            name = className;
            st.define(name, type, kind);
        }
        type = jk.token;

        jk.advance();
        name = jk.token;

        st.define(name, type, kind);
        jk.advance();

        while(jk.token.equals(",")){
            jk.advance();
            type = jk.token;
            jk.advance();
            name = jk.token;
            st.define(name,type,kind);
            jk.advance();
        }

        //fw.write("</parameterList>\n");
    }

    // our special jk.advance() call depends on if the 'ifstatement' has read
    // too much
    private void advanceBasedOnIf(JackTokenizer jk) throws IOException {
        if (inIf)  {
            inIf = false;
            return;
        }
        else {
            jk.advance();
        }
    }
    public void compileMethod(JackTokenizer jk) throws IOException{
        /* so far we haven't implemeneted the return-or-not checking on
         * compileMethod */
        String methodID = "";
        st.startSubroutine();

        if (jk.token.equals("constructor")) {
            isConstructor = true;
        } else {
            isConstructor = false;
        }

        jk.advance();

        // parse the return type of the method
        if (isConstructor && !jk.tokenType.equals("IDENTIFIER")) {
            error("constructor requires a class return-type");
        } 

        // set return type to void or not
        if (jk.token.equals("void")) {
            isVoid = true;
        } else 
            isVoid = false;

        jk.advance();

        // parse the name of the method
        if (isConstructor && !jk.token.equals("new")) {
            error("constructor requires new()");
        } else {
            // record method name here;
            methodID = jk.token;
        }
        jk.advance();

        // parse the argList of the method
        match(jk, "(");
        jk.advance();
        if (!jk.token.equals(")")){
            compileArgList(jk, methodID);
        }

        match(jk, ")");
        jk.advance();
        match(jk, "{");
        jk.advance();

        int i = 1;
        // run over the entire subroutineBody
        for(; !jk.token.equals("}"); advanceBasedOnIf(jk)) {

            // parse varlist
            if (jk.token.equals("var")) {
                compileVarDec(jk);
                match(jk,";");
            } else {
                // now write this function label at first with the number of
                // variables
                if (i>0) {
                    vmw.writeMethod(className, methodID, st.varCount("var"));
                    //if this is a constructor, allocate space for the fields
                    //and point THIS to the current object
                    if(isConstructor) {
                        vmw.writePush("constant", st.varCount("field"));
                        vmw.writeCall("Memory.alloc", 1);
                        vmw.writePop("pointer", 0);
                    } else {
                        // set up this segment
                        vmw.writePush("argument", 0);
                        vmw.writePop("pointer", 0);
                    }
                    i--;
                } 
                compileStatements(jk);
            }

        }
        match(jk, "}");
    }

    public void compileVarList(JackTokenizer jk) throws IOException {
        //record type information
        type = jk.token;
        compileType(jk);
        jk.advance();

        name = jk.token;
        compileIdentifier(jk);

        st.define(name, type, kind);
        //        st.printSymbol(name);

        jk.advance();


        while(jk.token.equals(",")){
            jk.advance();

            name = jk.token;
            st.define(name, type, kind);

            compileIdentifier(jk);
            jk.advance();
        }
        resetAttr();
    }



    public void compileStatements(JackTokenizer jk) throws IOException {
        switch(jk.token) {
            case "let":
                compileLetStatement(jk);
                break;
            case "while":
                compileWhileStatement(jk);
                break;
            case "do":
                compileDoStatement(jk);
                break;
            case "return":
                compileReturnStatement(jk);
                break;
            case "if":
                compileIfStatement(jk);
                break;
            default:
                System.out.println(jk.token);
                match(jk, ";"); // could be an empty statement
        }

    }

    public void compileLetStatement(JackTokenizer jk) throws IOException {
        //parse "let"
        compileKeyword(jk);
        jk.advance();

        // record identifier to pop into later
        String name = jk.token;
        typeLeft = st.typeOf(jk.token);

        // parsing array)
        if(jk.token.equals("[")) {
            match(jk, "[");
            jk.advance();
            compileExpression(jk);
            match(jk, "]");
            jk.advance();
        } 
        // we should be at = now anyway
        jk.advance();
        match(jk, "=");
        jk.advance();

        compileExpression(jk);

        popIdentifier(name);
        match(jk, ";");
    }
    public void compileWhileStatement(JackTokenizer jk) throws IOException {

        String fc = Integer.toString(flowCounter);
        vmw.writeLabel("WHILE_EXP"+fc);
        jk.advance();

        match(jk, "(");
        jk.advance();
        compileExpression(jk);
        match(jk, ")");

        vmw.write("not\n");
        vmw.writeIf("WHILE_END" + fc);

        jk.advance();
        match(jk, "{");
        jk.advance();
        compileStatements(jk);
        jk.advance();
        match(jk,"}");
        vmw.writeGoto("WHILE_EXP" + fc);
        vmw.writeLabel("WHILE_END" + fc);
        flowCounter++;
    }

    public void compileIfStatement(JackTokenizer jk) throws IOException {
        compileKeyword(jk);
        jk.advance();

        match(jk, "(");
        jk.advance();

        compileExpression(jk);
        match(jk, ")");


        //create labels here:
        String fc = Integer.toString(flowCounter);
        vmw.writeIf("IF_TRUE" + fc);
        vmw.writeGoto("IF_FALSE" + fc);

        jk.advance();
        match(jk, "{");
        vmw.writeLabel("IF_TRUE" + fc);
        jk.advance();
        compileStatements(jk);
        jk.advance();
        match(jk, "}");

        // test whether there is an else statement
        jk.advance();
        inIf = true;

        if (jk.token.equals("else")) {
            //.write("<elseStatement>\n");
            vmw.writeGoto("IF_END" + fc);
            vmw.writeLabel("IF_FALSE" + fc);
            jk.advance();

            match(jk, "{");
            jk.advance();
            compileStatements(jk);
            jk.advance();
            match(jk, "}");

            //.write("</elseStatement>\n");
            // if there is an else statement, we proceed as usual
            inIf = false;
            vmw.writeLabel("IF_END" + fc);
        } else {
            vmw.writeLabel("IF_FALSE" + fc);
        }
        //.write("</ifStatement>\n");
        flowCounter++;
    }

    public void compileReturnStatement(JackTokenizer jk) throws IOException {
        //.write("<returnStatement>\n");
        //compileKeyword(jk);
        jk.advance();

        if(!jk.token.equals(";")) {
            compileExpression(jk);
        }
        if(isVoid) {
            vmw.writePush("constant", 0);
        }
        vmw.write("return\n");
        match(jk, ";");
        //.write("</returnStatement>\n");

    }

    public void compileDoStatement(JackTokenizer jk) throws IOException {
        //compileKeyword(jk);
        jk.advance();

        // in case a method like id1.id2() is called
        String id2 = jk.token;
        String id1 = "";
        jk.advance();
        if (jk.token.equals(".")) {
            match(jk, ".");
            jk.advance();
            id1 = jk.token;
            //compileIdentifier(jk);
            jk.advance();
        }
        match(jk, "(");
        jk.advance();

        int numExpr = compileExpressionList(jk);

        match(jk, ")");
        jk.advance();
        match(jk, ";");

        if (id1.equals("")) { 
            //calling a method belonging to this:
            vmw.writePush("pointer", 0);
            vmw.writeCall(className + "." + id2, numExpr);

        } else {
            vmw.writeCall(id2+"."+id1, numExpr-1);
        }
        // doStatement does not need to return anything, so remove it from the
        // stack.
        vmw.writePop("temp", 0);
    }

    public void compileStringConst(JackTokenizer jk) throws IOException {
        //.write("<StringConstant>\n" + jk.token + "</StringConstant>\n");
    }

    public void compileIntConst(JackTokenizer jk) throws IOException {
        //.write("<IntConstant>\n" + jk.token + "</IntConstant>\n");
    }

    // after compileExpression returns, we want jk.token to be the token
    // after the expression. This is because expression does not have a natural
    // ending like statements ending with ;.
    public void compileExpression(JackTokenizer jk) throws IOException {
        compileTerm(jk);
        //jk.advance();
        if (isOp(jk.token)) {
            String op = jk.token;
            jk.advance();
            compileTerm(jk);
            writeOp(op);
        }
    }

    public void writeOp(String op) throws IOException {
        switch(op) {
            case "+":
                vmw.write("add\n");
                break;
            case "-":
                vmw.write("sub\n");
                break;
            case "*":
                vmw.writeCall("Math.multiply", 2);
                break;
            case "/":
                vmw.writeCall("Math.divide", 2);
                break;
            case "&":
                vmw.write("and\n");
                break;
            case "|":
                vmw.write("or\n");
                break;
            case "<":
                vmw.write("lt\n");
                break;
            case ">":
                vmw.write("gt\n");
                break;
            case "=":
                vmw.write("eq\n");
                break;
            default:
                error("unrecognized operator: " + op);
        }
    }
    public int compileExpressionList(JackTokenizer jk) throws IOException {
        int numExpr = 0;
        if (!jk.token.equals(")")) { 
            numExpr++;
            compileExpression(jk);
            //jk.advance();
            while(jk.token.equals(",")){
                match(jk, ",");
                jk.advance();
                compileExpression(jk);
                numExpr++;
            }
        } 
        numExpr++;       //there is a hidden argument
        return numExpr;      // empty argument list.

    }
    private boolean isOp(String s) {
        if (s.equals("+") || s.equals("-") || s.equals("*") 
                || s.equals("/") || s.equals("&") || s.equals("|") 
                || s.equals("<") || s.equals(">") || s.equals("="))
            return true;
        else return false;
    }
    public void compileTerm(JackTokenizer jk) throws IOException {
        if (jk.token.equals("-") || jk.token.equals("~")) {
            String s = jk.token;
            jk.advance();
            compileTerm(jk);
            //vmw.write(s+ "\n");
            if (s.equals("-")) {
                vmw.write("neg\n");
            } else {
                vmw.write("not\n");
            }
            return;
        }

        if (jk.tokenType.equals("STRING_CONST")) {
            compileStringConst(jk);
            jk.advance();
            return;
        }

        if (jk.tokenType.equals("INT_CONST")){
            vmw.writePush("constant", Integer.parseInt(jk.token));
            //compileIntConst(jk);
            //System.out.println(jk.token);
            jk.advance();
            return;
        }

        if (jk.token.equals("true")) {
            if (typeLeft.equals(null) || !typeLeft.equals("boolean")) {
                error("you cannot assign boolean type to a non-boolean variables");
            }
            vmw.writePush("constant", 1);
            vmw.write("neg\n");
            jk.advance();
            return;
        }

        if (jk.token.equals("false")) {
            if (typeLeft.equals(null) || !typeLeft.equals("boolean")) {
                error("you cannot assign boolean type to a non-boolean variable");
            }
            vmw.writePush("constant", 0);
            jk.advance();
            return;
        }

        if (jk.token.equals("this")) {
            vmw.writePush("pointer", 0);
            jk.advance();
            return;
        }

        if (jk.token.equals("(")) {
            match(jk, "(");
            jk.advance();

            // we do not call advance after compileExpression
            compileExpression(jk);

            match(jk, ")");
            jk.advance();
            return;
        }

        // now we are left with varName, varName(...), varName.xxx, var[...]


        // id must be an identifier, but we need to tell if it is a variable or
        // method call etc
        String id = jk.token;
        jk.advance();

        switch(jk.token) {
            // array, treated just as usual
            case "[":
                match(jk, "[");
                jk.advance();
                compileExpression(jk);  // do not call advance() afterwards;
                match(jk, "]");
                jk.advance();
                break;

                // this is a method call
            case "(":
                //right now we do not check if the method is valid, we will
                //modify the compiler into building method table at first before
                //parsing.
              //  if(st.typeOf(className + "." +  id) != "method") {
              //      error("unknown method name: " + className + "." + id);
              //  } 
                vmw.writePush("pointer", 0);
                match(jk, "(");
                jk.advance();
                int numExpr =  compileExpressionList(jk);
                match(jk, ")");
                jk.advance();

                vmw.writeCall(className + "." +  id, numExpr);
                break;

            case ".":
                match(jk, ".");
                jk.advance();
                String id1 = jk.token;
                jk.advance();
                match(jk, "(");
                jk.advance();
                numExpr = compileExpressionList(jk);
                match(jk, ")");
                jk.advance();

                vmw.writeCall(id+"."+id1, numExpr-1);
                break;
            default:
                // "let var = id"
                pushIdentifier(id);
        }
    }
    public void popIdentifier(String name) throws IOException {
        String k = st.kindOf(name);
        if (k == null) {
            error("Unrecognized identifier: " + name);
        } else {
            String seg = "";
            switch(k) {
                case "argument":
                    seg = "argument";
                    break;
                case "field":
                    seg = "this";
                    break;
                case "var":
                    seg = "local";
                    break;
                case "static":
                    seg = "static";
                    break;
                default:
                    error("unrecognized kind of " + name);
            }
            vmw.writePop(seg, st.indexOf(name));
        }

    }
    public void pushIdentifier(String name) throws IOException {
        String k = st.kindOf(name);
        if (k == null) {
            error("Unrecognized identifier: " + name);
        } else {
            String seg = "";
            switch(k) {
                case "argument":
                    seg = "argument";
                    break;
                case "field":
                    seg = "this";
                    break;
                case "var":
                    seg = "local";
                    break;
                case "static":
                    seg = "static";
                    break;
                default:
                    error("unrecognized kind of " + name);
            }
            vmw.writePush(seg, st.indexOf(name));
        }

    }


    }


