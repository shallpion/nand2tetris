import java.util.*;


class SymbolTable {

    private class SymbolAttr {
        public String type;
        public String kind;
        public int index;

        public SymbolAttr(String type, String kind, int index) {
            this.type = type;
            this.kind = kind;
            this.index = index;
        }
    }

    public HashMap<String, SymbolAttr> classTable;
    public HashMap<String, SymbolAttr> subroutineTable;

    // count for class variables
    public int staticCount;
    public int fieldCount;

    // count for subroutine variables
    public int varCount;
    public int argCount;

    public SymbolTable() {
        classTable = new HashMap<String, SymbolAttr>();
        subroutineTable = new HashMap<String, SymbolAttr>();

        staticCount = 0;
        fieldCount = 0;
        varCount = 0;
        argCount = 0;
    }


    void define(String name, String type, String kind) {
        int id = 0;
        boolean classScope = false;
        switch(kind) {
            case "argument":
                id = argCount;
                argCount++;
                break;
            case "var":
                id = varCount;
                varCount++;
                //System.out.println("woof");
                break;
            case "field":
                id = fieldCount;
                fieldCount++;
                classScope = true;
                break;
            case "static":
                id = staticCount;
                staticCount++;
                classScope = true;
                break;
        }
        SymbolAttr sa = new SymbolAttr(type, kind, id);
        if (classScope) {

            classTable.put(name, sa);
        } else
            subroutineTable.put(name, sa);

    }

    public int varCount(String kind) {
        switch(kind) {
            case "argument":
                return argCount;
            case "var":
                return varCount;
            case "field":
                return fieldCount;
            case "static":
                return staticCount;
            default:
                throw new IllegalArgumentException("no such count");
        }
    }

    public String kindOf(String name) {
        SymbolAttr sa = subroutineTable.get(name);
        if (sa != null) {
            return sa.kind;
        } 
        sa = classTable.get(name);
        if (sa != null) {
            return sa.kind;
        }

        return null;
    }

    public String typeOf(String name) {
        SymbolAttr sa = subroutineTable.get(name);
        if (sa != null) {
            return sa.type;
        } 
        sa = classTable.get(name);
        if (sa != null) {
            return sa.type;
        }

        return null;
    }


    public int indexOf(String name) {
        SymbolAttr sa = subroutineTable.get(name);
        if (sa != null) {
            return sa.index;
        }
        sa = classTable.get(name);
        if (sa != null) {
            return sa.index;
        }

        return -1;
    }

    void startSubroutine() {
        subroutineTable.clear();
        varCount = 0;
        argCount = 0;
    }
    public void printSymbol(String name) {
        SymbolAttr sa;
        sa = subroutineTable.get(name);
        if (sa != null) {
            System.out.println("subroutine symbol added!\nname: " + name + "\n" + 
                    "type: " + sa.type + "\n" +
                    "kind: " + sa.kind + "\n" + 
                    "index: " + Integer.toString(sa.index) + "\n");

        } else {
            sa = classTable.get(name);
            if (sa != null) {
                System.out.println("class symbol added!\nname: " + name + "\n" + 
                        "type: " + sa.type + "\n" +
                        "kind: " + sa.kind + "\n" + 
                        "index: " + Integer.toString(sa.index) + "\n");

            } else
                System.out.println("no symbol entry for \""+ name + "\" found");
        }
    }

}
