import java.io.*;

class VMWriter{
    private String fileName;
    private FileWriter fw;

    public VMWriter(String file) {
        fileName = file.substring(file.indexOf("/") + 1, file.lastIndexOf("."));
        try {
            fw = new FileWriter(fileName + ".vm");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getName() {
        return fileName;
    }
    // Segment: CONST, ARG, ...
    public void writePush(String segment, int index) throws IOException {
        fw.write("push " + segment + " " + index + "\n");
    }

    public void writePop(String segment, int index) throws IOException {
        fw.write("pop " + segment + " " + index + "\n");
    }

    public void writeArithmetic(String command) throws IOException {
        fw.write(command + "\n");
    }

    public void writeLabel(String label) throws IOException {
        fw.write("label " + label + "\n");
    }

    public void writeGoto(String label) throws IOException {
        fw.write("goto " + label + "\n");
    }

    public void writeIf(String label) throws IOException {
        fw.write("if-goto " + label + "\n");
    }

    public void writeCall(String name, int nArgs) throws IOException {
        fw.write("call " + name + " "+ Integer.toString(nArgs) + "\n");
    }

    public void writeFunction(String name, int nLocals) throws IOException {
        fw.write("function " + name + " " + Integer.toString(nLocals) + "\n");
    }

    public void writeMethod(String classBelongedTo, String name, int nLocals)
        throws IOException {
        fw.write("function " + classBelongedTo + "." + name + " " +
                Integer.toString(nLocals) + "\n");
    }
    public void writeReturn() throws IOException {
        fw.write("return\n");
    }

    public void writeComment(String comment) throws IOException {
        fw.write("// " + comment + "\n");
    }
    public void write(String s) throws IOException {
        fw.write(s);
    }
}
