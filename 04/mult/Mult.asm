// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.

// The implementation works like this: R0 * R1 = R0 + R0 + ... (R1s) + R0

@2	// Put R2 into M
M=0	// Zero it


@1
D = M
@3
M = D 	// Store R1 in R3 as temp variable

@0
D = M
@END
D;JEQ	// If R0 = 0, return

(LOOP)
@3
D = M
@END
D;JEQ	// If R1 = 0, stop

@0
D = M	// Put R0 into D
@2
M = D + M	// Put one additional copy of R0 into R2

@3
M = M - 1	// Decraese R1 by 1

@LOOP
0;JMP


(END)
@END
0;JMP

