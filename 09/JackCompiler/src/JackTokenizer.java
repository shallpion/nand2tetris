import java.io.*;

class JackTokenizer {
    //public String lookAhead;
    public char peek;
    public String token;
    public String tokenType;
    public BufferedReader br;

    private char readBuffer;

    private boolean inStringConst;
    public boolean eof;

    public JackTokenizer(String file) {
        peek = ' ';
        readBuffer = ' ';
        tokenType = "";
        token = "";
        inStringConst = false;

        eof = false;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }


    public int readch() throws IOException {
        int c ; 
        if (readBuffer != ' ') {
            c = (int) readBuffer;
            readBuffer = ' ';
        } else 
            c = br.read();
        return c;
    }
    public void advance() throws IOException {
        // reset tokenType
        tokenType = "";
        token = "";

        int c;
        char temp = ' ';
        while ((c = readch()) != -1) {
            //System.out.println(inStringConst);
            peek = (char) c;

            // String_Const case
            if (peek == '"') {
                int t = 0;
                StringBuffer d = new StringBuffer();
                while((t = br.read()) != -1 && (char) t != '"') {
                    d.append((char) t);
                }
                token = d.toString();
                tokenType = "STRING_CONST";
                return;
            }

            // Ignore spaces, new lines
            if (peek == ' ' || peek == '\t' || peek == '\n') continue;

            // Ignore "//comments... \n"
            if (peek == '/' && (temp = (char) readch()) == '/') {
                while ((peek=(char) readch()) != '\n') {}
                continue;
            } else if (peek == '/' ) readBuffer = temp;

            // Ignore "/* ... */"
            if (peek == '/' && (temp=(char) readch()) == '*') {
                while ((peek = (char) readch()) != '*' || (peek=(char)readch()) != '/') {}
                continue;
            } else if (peek == '/') readBuffer = temp; // it could be a/b, a division.

            if (Character.isLetter(peek)) {
                //                readBuffer = ' ';
                StringBuffer b = new StringBuffer();
                do {
                    b.append(peek);
                    peek = (char)readch();
                } while(Character.isLetterOrDigit(peek) || peek == '_');

                // save peek
                readBuffer = peek;
                token = b.toString();
                if (JackAnalyzer.keyword.contains(token)) {
                    tokenType = "KEYWORD";
                } else tokenType = "IDENTIFIER";
                return;
            }

            if (Character.isDigit(peek)) {
                StringBuffer b = new StringBuffer();
                do {
                    b.append(peek);
                    peek = (char)readch();
                } while(Character.isDigit(peek));
                readBuffer = peek;

                token = b.toString();
                tokenType = "INT_CONST";
                return;
            }

            // peek is anything else, like +-*/
            token = peek + "";

            //            if (peek != '"') {
            //               token = peek + "";
            //            } else {
            //                inStringConst = true;
            //            }
            //     if (token.equals("\"") && readBuffer != '"') {
            //         inStringConst = true;
            //     //           System.out.println("woof");
            //     }


            if(JackAnalyzer.symbol.contains(token)) {
                tokenType = "SYMBOL";
            } 

            return;
        }

        // if we have reached here, eof is reached.
        eof = true;
    }


}











