import java.util.*;


    /* nestedTable:  tableEntry-->tableEntry-->tableEntry-->
        tableEntry:  {scope, comment}
             scope:  HashMap<identifier, keyContent>
    */



class SymbolTable {
    // sub-class for storing values in the tableEntries
    public class keyContent {
        public String type;
        public String kind;
        public int number;

        public keyContent(String type, String kind, int number) {
            this.type = type;
            this.kind = kind;
            this.number = number;
        }
    }

    // class-wrapper for each entry in the linked list. comment string is for
    // the convenience of debugging
    public class tableEntry {
        public HashMap<String, keyContent> scope;
        public String comment;

        public tableEntry(String comment) {
            scope = new HashMap<String, keyContent>();
            this.comment = comment;
        }
    }

    public LinkedList<tableEntry> nestedTable;

    public SymbolTable(String comment) {
        tableEntry rootScope = new tableEntry(comment);
        nestedTable = new LinkedList<tableEntry>();
        nestedTable.addFirst(rootScope);
    }

    public void newScope(String comment) {
        nestedTable.add(new tableEntry(comment));
    }

    public void delScope() {
        nestedTable.removeLast();
    }

    public void addEntry(String id, String type, String kind, int number) {

        tableEntry current = nestedTable.getLast();
        keyContent tc = new keyContent(type, kind, number);
        current.scope.put(id, tc);
    }

    public keyContent searchInCurrentScope(String id) {
        // return null if not found
        return nestedTable.getLast().scope.get(id);
    }

    public keyContent searchGlobally(String id) {
        Iterator<tableEntry> it = nestedTable.descendingIterator();
        while (it.hasNext()) {
            keyContent kc = it.next().scope.get(id);
            if (kc != null) {
                return kc;
            } 
        }

        return null;
    }
}


