
import java.io.*;
import java.util.*;

class JackAnalyzer {
 //   public static Set<String> id;
    public static Set<String> keyword;
    public static Set<String> symbol;
    public FileWriter fw = null;

    static{
        keyword = new HashSet<String>();
        keyword.add("class");       keyword.add("constructor");
        keyword.add("function");    keyword.add("method");
        keyword.add("field");       keyword.add("static");
        keyword.add("var");         keyword.add("int");
        keyword.add("char");        keyword.add("boolean");
        keyword.add("void");        keyword.add("true");
        keyword.add("false");       keyword.add("null");
        keyword.add("this");        keyword.add("let");
        keyword.add("do");          keyword.add("if");
        keyword.add("else");        keyword.add("while");
        keyword.add("return");
    }

    static {
        symbol = new HashSet<String>();
        symbol.add("{");        symbol.add("}");
        symbol.add("(");        symbol.add(")");
        symbol.add("[");        symbol.add("]");
        symbol.add(".");        symbol.add(",");
        symbol.add(";");        symbol.add("+");
        symbol.add("*");        symbol.add("/");
        symbol.add("&");        symbol.add("|");
        symbol.add("<");        symbol.add(">");
        symbol.add("=");        symbol.add("~");
    }


    public static void main(String files[]) {
        if (files.length == 0) {
            System.out.println("Usage: java JackAnalyzer *.jack");
            return;
        }

        for (String file : files) {
            JackTokenizer jk = new JackTokenizer(file);
            CompilationEngine ce = new CompilationEngine(file);
            try {
                jk.advance();
                ce.compileClass(jk);
//
//                while(!jk.eof) {
//                    //  System.out.println("woof");
//                    //System.out.print(jk.tokenType + "\t");
//                    System.out.println(String.format("%-30s %-10s", jk.tokenType,jk.token));
//                    //System.out.println(jk.token);
//                    jk.advance();
//                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            ce.close();
        }
    }
}
