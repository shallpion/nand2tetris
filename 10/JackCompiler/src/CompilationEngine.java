import java.io.*;

class CompilationEngine {
    public String fileName;
    private FileWriter fw;

    // className is used for several purposes.
    private String className;

    // this is a special variable to tell if we are in the 'if' staetment, because
    // we have to test whether 'if' is followed by an else, therefore we have to
    // figure out a way to "skip" an advance() call if in the 'if' statement
    private boolean inIf;



    public CompilationEngine (String file) {
        fileName = file.substring(file.indexOf("/")+1, file.lastIndexOf("."));
        try {
            fw = new FileWriter(fileName + ".xml");
        } catch(IOException e) {
            e.printStackTrace();
        } 
    }

    public void close() {
        try {
            fw.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    // This method is use to match simple symbols like { or so.
    public void match(JackTokenizer jk, String s) throws IOException {
        if(!jk.token.equals(s)) {
            throw new IllegalStateException("expecting a symbol" + s);
        } else {
            fw.write("<symbol>" + s + "</symbol>\n");
        }
    }

    public void compileClass (JackTokenizer jk) throws IOException {
        if (!jk.token.equals("class")) {
            throw new IllegalStateException("no class found");
        } else {
            fw.write("<class>\n<keyword>class</keyword>\n");
        }

        // move on
        jk.advance();

        // parse the identifier of the class
        compileIdentifier(jk);
        className = jk.token;
        jk.advance();

        // parse the { block
        match(jk, "{");
        jk.advance();

        for(;!jk.token.equals("}"); jk.advance()){
            if(jk.token.equals("field") || jk.token.equals("static")) {

                compileVarDec(jk);

            } else if (jk.token.equals("constructor")) {

                compileMethod(jk);

            } else if (jk.token.equals("method")) {

                compileMethod(jk);
            }

        }

        match(jk, "}");

        fw.write("</class>\n");
        }

    public void compileIdentifier(JackTokenizer jk) throws IOException {
        if (!jk.tokenType.equals("IDENTIFIER")){
            throw new IllegalStateException("expecting identifier");
        } else {
            fw.write("<identifier>" + jk.token + "</identifier>\n");
        }
    }

    public void compileKeyword(JackTokenizer jk) throws IOException {
        if (!jk.tokenType.equals("KEYWORD")) {
            throw new IllegalStateException("expecting keyword");
        } else {
            fw.write("<keyword>" + jk.token + "</keyword>\n");
        }
    }

    public void compileType(JackTokenizer jk) throws IOException {
        if(JackAnalyzer.keyword.contains(jk.token)){
            compileKeyword(jk);
        }  else {
            // this maybe a class type variable
            compileIdentifier(jk);
        }

    }

    public void compileVarDec(JackTokenizer jk) throws IOException {
        fw.write("<classVarDec>\n");
        compileKeyword(jk);
        jk.advance();

        compileVarList(jk);

        match(jk, ";");
        fw.write("</classVarDec>\n");

    }

    public void compileVarList(JackTokenizer jk) throws IOException {
        // type of variable(s);
        compileType(jk);
        jk.advance();

        compileIdentifier(jk);
        jk.advance();

        while(jk.token.equals(",")){
            jk.advance();
            compileIdentifier(jk);
            jk.advance();
        }
    }

    public void compileArgList(JackTokenizer jk) throws IOException {
        fw.write("<parameterList>\n");
        compileType(jk);
        jk.advance();

        compileIdentifier(jk);
        jk.advance();

        while(jk.token.equals(",")){
            jk.advance();
            compileType(jk);
            jk.advance();
            compileIdentifier(jk);
            jk.advance();
        }

        fw.write("</parameterList>\n");
    }

    // our special jk.advance() call depends on if the 'ifstatement' has read
    // too much
    private void advanceBasedOnIf(JackTokenizer jk) throws IOException {
        if (inIf)  {
            inIf = false;
            return;
        }
        else {
            jk.advance();
        }
    }
    public void compileMethod(JackTokenizer jk) throws IOException{
        fw.write("<subroutineDec>\n");
        compileKeyword(jk);
        jk.advance();

        // parse the return type of the method
        compileType(jk);
        jk.advance();

        // parse the name of the method
        compileIdentifier(jk);
        jk.advance();

        // parse the argList of the method
        match(jk, "(");
        jk.advance();
        if (!jk.token.equals(")")){
            compileArgList(jk);
        }

        match(jk, ")");
        jk.advance();
        match(jk, "{");
        jk.advance();

        // ran over the entire subroutineBody
        //for(; !jk.token.equals("}"); jk.advance()) {
        for(; !jk.token.equals("}"); advanceBasedOnIf(jk)) {

            // parse varlist
            if (jk.token.equals("var")) {
                fw.write("<varDec>\n");
                compileKeyword(jk);
                jk.advance();
                compileVarList(jk);
                fw.write("</varDec>\n");
                match(jk,";");
            } else compileStatements(jk);

        }

        match(jk, "}");
        fw.write("</subroutineDec>\n");
        }



    public void compileStatements(JackTokenizer jk) throws IOException {
        switch(jk.token) {
            case "let":
                compileLetStatement(jk);
                break;
            case "while":
                compileWhileStatement(jk);
                break;
            case "do":
                compileDoStatement(jk);
                break;
            case "return":
                compileReturnStatement(jk);
                break;
            case "if":
                compileIfStatement(jk);
                break;
            default:
                System.out.println(jk.token);
                match(jk, ";"); // could be an empty statement
        }

    }

    public void compileLetStatement(JackTokenizer jk) throws IOException {
        //parse "let"
        fw.write("<letStatement>\n");
        compileKeyword(jk);
        jk.advance();

        // parse identifier
        compileIdentifier(jk);
        jk.advance();

        // parsing array)
        if(jk.token.equals("[")) {
            match(jk, "[");
            jk.advance();
            compileExpression(jk);
            match(jk, "]");
            jk.advance();
        } 
        // we should be at = now anyway
        match(jk, "=");
        jk.advance();

        compileExpression(jk);
        //System.out.println(jk.token);
        match(jk, ";");
        fw.write("</letStatement>\n");
    }

    public void compileWhileStatement(JackTokenizer jk) throws IOException {
        fw.write("<whileStatement>\n");
        compileKeyword(jk);
        jk.advance();

        match(jk, "(");
        jk.advance();

        compileExpression(jk);
        match(jk, ")");
        jk.advance();
        match(jk, "{");
        jk.advance();
        compileStatements(jk);
        jk.advance();
        match(jk,"}");

        fw.write("</whileStatement>\n");
    }

    public void compileIfStatement(JackTokenizer jk) throws IOException {
        fw.write("<ifStatement>\n");
        compileKeyword(jk);
        jk.advance();

        match(jk, "(");
        jk.advance();

        compileExpression(jk);
        match(jk, ")");
        jk.advance();
        match(jk, "{");
        jk.advance();
        compileStatements(jk);
        jk.advance();
        match(jk, "}");

        // test whether there is an else statement
        jk.advance();
        inIf = true;

        if (jk.token.equals("else")) {
            fw.write("<elseStatement>\n");
            jk.advance();

            match(jk, "{");
            jk.advance();
            compileStatements(jk);
            jk.advance();
            match(jk, "}");

            fw.write("</elseStatement>\n");
            // if there is an else statement, we proceed as usual
            inIf = false;
        }
        fw.write("</ifStatement>\n");
    }

    public void compileReturnStatement(JackTokenizer jk) throws IOException {
        fw.write("<returnStatement>\n");
        compileKeyword(jk);
        jk.advance();

        if(!jk.token.equals(";")) {
            compileExpression(jk);
        }
        match(jk, ";");
        fw.write("</returnStatement>\n");

    }

    public void compileDoStatement(JackTokenizer jk) throws IOException {
        fw.write("<doStatement>\n");
        compileKeyword(jk);
        jk.advance();

        fw.write("<subroutineCall>\n");
        compileIdentifier(jk);
        jk.advance();
        if (jk.token.equals(".")) {
            match(jk, ".");
            jk.advance();
            compileIdentifier(jk);
            jk.advance();
        }
        match(jk, "(");
        jk.advance();
        compileExpressionList(jk);
        match(jk, ")");
        jk.advance();
        match(jk, ";");
        fw.write("</subroutineCall>\n");
        fw.write("</doStatement>\n");
    }

    public void compileStringConst(JackTokenizer jk) throws IOException {
        fw.write("<StringConstant>\n" + jk.token + "</StringConstant>\n");
    }

    public void compileIntConst(JackTokenizer jk) throws IOException {
        fw.write("<IntConstant>\n" + jk.token + "</IntConstant>\n");
    }

    // after compileExpression returns, we want jk.token to be the token
    // after the expression. This is because expression does not have a natural
    // ending like statements ending with ;.
    public void compileExpression(JackTokenizer jk) throws IOException {
        fw.write("<expression>\n");
        compileTerm(jk);
        //jk.advance();
        if (isOp(jk.token)) {
            match(jk, jk.token);
            jk.advance();
            compileTerm(jk);
        }
        //System.out.println(jk.token);
        fw.write("</expression>\n");
    }
    public void compileExpressionList(JackTokenizer jk) throws IOException {
        if (!jk.token.equals(")")) { 
            fw.write("<expressionList>\n");
            compileExpression(jk);
            //jk.advance();
            while(jk.token.equals(",")){
                match(jk, ",");
                jk.advance();
                compileExpression(jk);
                //   jk.advance();
            }
            fw.write("</expressionList>\n");
        } else return;      // empty argument list.

    }
    private boolean isOp(String s) {
        if (s.equals("+") || s.equals("-") || s.equals("*") 
                || s.equals("/") || s.equals("&") || s.equals("|") 
                || s.equals("<") || s.equals(">") || s.equals("="))
            return true;
        else return false;
    }
    public void compileTerm(JackTokenizer jk) throws IOException {
        //    if (jk.token.equals(")")) {
        //        return;     //empty expression;
        //    }
        if (jk.token.equals("-") || jk.token.equals("~")) {
            match(jk, jk.token);
            jk.advance();
            compileTerm(jk);
            return;
        }

        if (jk.tokenType.equals("STRING_CONST")) {
            compileStringConst(jk);
            jk.advance();
            return;
        }

        if (jk.tokenType.equals("INT_CONST")){
            compileIntConst(jk);
            //System.out.println(jk.token);
            jk.advance();
            return;
        }
        switch(jk.token) {
            case "true":
            case "false":
            case "null":
            case "this":
                compileKeyword(jk);
                jk.advance();
                return;
        }
        if (jk.token.equals("(")) {
            match(jk, "(");
            jk.advance();

            // we do not call advance after compileExpression
            compileExpression(jk);

            match(jk, ")");
            jk.advance();
            return;
        }

        // now we are left with varName, varName(...), varName.xxx, var[...]


        //compileIdentifier(jk);
        // id must be an identifier, but we need to tell if it is a variable or
        // method call etc
        String id = jk.token;
        jk.advance();

        switch(jk.token) {
            // array, treated just as usual
            case "[":
                fw.write("<identifier>" + id + "</identifier>\n");
                match(jk, "[");
                jk.advance();
                compileExpression(jk);  // do not call advance() afterwards;
                match(jk, "]");
                jk.advance();
                break;
            case "(":
                fw.write("<subroutineCall>\n");
                fw.write("<identifier>" + id + "</identifier>\n");
                //compileIdentifier(jk);
                //jk.advance();
                match(jk, "(");
                jk.advance();
                compileExpressionList(jk);
                //System.out.println(jk.token);
                match(jk, ")");
                fw.write("</subroutineCall>\n");
                jk.advance();
                break;
            case ".":
                fw.write("<subroutineCall>\n");
                fw.write("<identifier>" + id + "</identifier>\n");
                //compileIdentifier(jk);
                //jk.advance();
                match(jk, ".");
                jk.advance();
                compileIdentifier(jk);
                jk.advance();
                match(jk, "(");
                jk.advance();
                compileExpressionList(jk);
                match(jk, ")");
                fw.write("</subroutineCall>\n");
                jk.advance();
            default:
                fw.write("<identifier>" + id + "</identifier>\n");
        }
    }

















    }


