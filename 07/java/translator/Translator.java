package translator;
import parser.*;
import asmwriter.*;
import java.io.*;

public class Translator {
    public static void main(String args[]) throws IOException {
        if (args.length == 0) {
            System.out.println("Usage: Translator *.vm");
            return;
        }

        Parser parser = new Parser(args[0]);
        AsmWriter writer = new AsmWriter(args[0]);

        while(parser.hasMoreCommands) {
            parser.advance();
            //Parser.commandType a = parser.commandType();
            switch(parser.commandType()) {
                case C_ARITHMETIC:
                    writer.fw.write(writer.writeArithmetic(parser));
                    break;
                case C_PUSH:
                    writer.fw.write(writer.writePush(parser));
                    break;
                case C_POP:
                    writer.fw.write(writer.writePop(parser));
                    break;
            }
        }
        writer.fw.close();

    }
}

