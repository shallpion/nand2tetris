package parser;
import java.io.*;
import java.util.*;

public class Parser {
    public Boolean hasMoreCommands = false;

    /* Set up buffer to read files line by line */
    //private File file;
    private Scanner scan;
    public Parser(String in) {
        File file = new File(in);
        try {
            scan = new Scanner(file);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();

        }
        if (scan.hasNextLine()) {
            hasMoreCommands = true;
        }
    }

    /* Parse the files line by line */
    public String command = "";
    public String arg1 = "";
    public String arg2 = "";
    public void advance() {

        // clear the buffer before reading the next command.
        command = "";
        arg1 = "";
        while(hasMoreCommands && command.equals("")) {
            command = scan.nextLine();
            command = command.replaceAll("//.*", "").trim();
            hasMoreCommands = scan.hasNextLine();
        }
        if (!hasMoreCommands) {
            scan.close();
        }
    }

    public enum commandType {
        C_ARITHMETIC,       C_PUSH,     C_POP,
        C_LABEL,            C_GOTO,     C_IF,
        C_FUNCTION,         C_RETURN,   C_CALL,
        C_ERROR     // catch error

    }

    
    public commandType commandType() {
        String token[] = command.split(" ");
        switch(token[0]) {
            case "add":
            case "sub":
            case "neg":
            case "eq":
            case "gt":
            case "lt":
            case "and":
            case "or":
            case "not":
                arg1 = token[0];
                return commandType.C_ARITHMETIC;
            case "push":
                arg1 = token[1];
                arg2 = token[2];
                return commandType.C_PUSH;
            case "pop":
                arg1 = token[1];
                arg2 = token[2];
                return commandType.C_POP;
            default:
                //System.out.println("nothing\n");
                return commandType.C_ERROR;
        }
    }

}
























