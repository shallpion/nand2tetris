package asmwriter;
import parser.*;
import java.io.*;

public class AsmWriter {
    private static int lbl;     // used for jumping command
    private String fileName;

    public String CR = System.getProperty("line.separator");

    public FileWriter fw = null;
    public AsmWriter(String in) {
        fileName = in.substring(0, in.lastIndexOf("."));
        System.out.println("output:" + fileName + ".asm");
        try {
            fw = new FileWriter(fileName + ".asm");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public String writeArithmetic(Parser parser) {
        switch(parser.arg1) {
            case "add":
                return 
                    "@SP" + CR +
                    "AM=M-1" + CR +     //AM=... instead of A=... 
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "M=D+M" + CR + CR;  //aditional empty line for easy reading
            case "sub":
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +    
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "M=M-D" + CR + CR;
            case "neg":
                return
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=-M" + CR + CR;
            case "and":
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "M=D&M" + CR + CR;
            case "or":
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "M=D|M" + CR + CR;
            case "not":
                return
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=!M" + CR + CR;
            case "eq":
                String s = lbl++ + "";
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "D=M-D" + CR +
                    "@EQ.TRUE" + s + CR +
                    "D;JEQ" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=0" + CR +
                    "@EQ.AFTER" + s + CR +
                    "(EQ.TRUE" + s + ")" + CR +
                    "0;JMP" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR + 
                    "M=-1" + CR +
                    "(EQ.AFTER" + s + ")" + CR + CR;
            case "gt":
                String r = lbl++ + "";
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "D=M-D" + CR +
                    "@GT.TRUE" + r + CR +
                    "D;JGT" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=0" + CR +
                    "@GT.AFTER" + r + CR +
                    "(GT.TRUE" + r + ")" + CR +
                    "0;JMP" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR + 
                    "M=-1" + CR +
                    "(GT.AFTER" + r + ")" + CR + CR;
            case "lt":
                String t = lbl++ + "";
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "D=M-D" + CR +
                    "@LT.TRUE" + t + CR +
                    "D;JLT" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=0" + CR +
                    "@LT.AFTER" + t + CR +
                    "(LT.TRUE" + t + ")" + CR +
                    "0;JMP" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR + 
                    "M=-1" + CR +
                    "(LT.AFTER" + t + ")" + CR + CR;
            default:
                return "";
        }
    }

    private String PUSH = 
        "@SP" + CR +
        "A=M" + CR +
        "M=D" + CR +
        "@SP" + CR +
        "M=M+1" + CR + CR;

    
    public String writePush(Parser parser) {
        switch(parser.arg1) {
            case "constant":
                return 
                    "@" + parser.arg2 + CR +
                    "D=A" + CR + 
                    PUSH;
            case "this":        //Push @THIS+arg2 on stack
                return
                    "@THIS" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH;
            case "that":        //Push @THIS+arg2 on stack
                return
                    "@THAT" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH;
            case "static":
                return
                    "@" + fileName + parser.arg2 + CR +
                    "D=M"+ CR +
                    PUSH;
            case "temp":
                return
                    "@R5" + CR +
                    "D=A" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH;
            case "pointer":
                if (parser.arg2.equals("0"))
                    return
                        "@THIS" + CR +
                        "D=M" + CR +
                        PUSH;
                else
                    return
                        "@THAT" + CR +
                        "D=M" + CR +
                        PUSH;
            case "local":        
                return
                    "@LCL" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH;
            case "argument":        
                return
                    "@ARG" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH;
            default:
                return "";
        }
    }

    private String POP = 
        "@R13" + CR +
        "M=D" + CR +      //store D's address at temp R13
        "@SP" + CR +
        "AM=M-1" + CR + 
        "D=M" + CR +
        "@R13" + CR +
        "A=M" + CR +
        "M=D" + CR + CR;

    public String writePop(Parser parser) {
        switch(parser.arg1) {
            case "this":        
                return
                    "@THIS" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP;
            case "that":        
                return
                    "@THAT" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP;
            case "static":
                return
                    "@" + fileName + parser.arg2 + CR +
                    "D=A" + CR +
                    POP;
            case "temp":
                return
                    "@R5" + CR +
                    "D=A" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP;
            case "pointer":
                if (parser.arg2.equals("0"))
                    return
                        "@THIS" + CR +
                        "D=A" + CR +
                        POP;
                else
                    return
                        "@THAT" + CR +
                        "D=A" + CR +
                        POP;
            case "local":        
                return
                    "@LCL" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP;
            case "argument":        
                return
                    "@ARG" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP;
            default:
                return "";

        }
    }
}
