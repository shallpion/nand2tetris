package parser;
import java.io.*;
import java.lang.*;
import instruction.*; import type.*; import symbol.*;

public class Parser {
    public String shortLine = "";
    public String binaryCode = "";

    // program counter, used to count the idx of program
    // instructions. Notice that it points to somewhere in the instruction
    // memory
    public static int pc = 0;
    
    // idx for the variables, notice where it starts
    public static int var = 16;

    public Parser(String inputLine) {

        // remove whitespaces and comments, if shortLine is empty, then it is
        // NOT an instruction.
        for (int i = 0; i < inputLine.length(); i++) {
            if (Character.isWhitespace(inputLine.charAt(i))) { continue; }
            else if (i < inputLine.length()-1 && inputLine.charAt(i) == '/' && inputLine.charAt(i+1)
                    == '/') 
            {
                break;
            } 
            else shortLine = shortLine + inputLine.charAt(i);
        }

    }

    // First pass to build symbol table
    public void buildSymbol(Symbol symbol) {
        if (shortLine.charAt(0) == '(') { 
            String s = shortLine.substring(1, shortLine.length()-1);
            if (symbol.symTable.containsKey(s)) {
                throw new IllegalStateException("duplicated symbol");
            } else {
                symbol.symTable.put(s, pc);

                // For debug reason we output s' memory location
                Integer i = symbol.symTable.get(s);
                String temp = "0";
                temp += String.format("%15s", Integer.toBinaryString(i)).replace(' ', '0');
                System.out.println(temp);
                // Once we ran into (Xxx), it does not take space in ROM, so we
                // descrease pc to compensate the pc++ in Assembler.java
                pc--;

            }

        }

    }

    // Second pass to parse the short line and return command type
    public Instruction commandType(Symbol symbol) {
        if (shortLine.charAt(0) == '@') 
        {
            String value = shortLine.substring(1);
            // If it is not a number, search to see if it is in the symbol
            // table, if not then add it (it means it is @var) to the symbol
            // table.
            if (!value.matches("\\d+")) {
                if (!symbol.symTable.containsKey(value)) {
                    
                    // We add this symbol into table with memory location at
                    // var,
                    symbol.symTable.put(value, var++);
                }
            }
            Instruction instruction = new Instruction(Type.A_COMMAND, value);
            return instruction;
        }
        else if (shortLine.charAt(0) == '(') {
            Instruction instruction = new Instruction(Type.A_COMMAND, null);
            return instruction;
        }
        else {
            String dest = "";
            String comp = "";
            String jump = "";

            int posEq = shortLine.indexOf('=');
            int posSemiCol = shortLine.indexOf(';');
            //System.out.println(posSemiCol);
            //posSemiCol = (posSemiCol < 0) ? shortLine.length() : posSemiCol;
            //System.out.println("posEq: " + posEq);
            //System.out.println("posSemiCol: " + posSemiCol);
            if (posEq >= 1) {
                dest = shortLine.substring(0, posEq);
            }
            if (posSemiCol < 0) {
                comp = shortLine.substring(posEq+1, shortLine.length());
            } else {
                comp = shortLine.substring(posEq+1, posSemiCol);
                jump = shortLine.substring(posSemiCol+1, shortLine.length());
            }

            //System.out.println("comp: " + comp);
            Instruction instruction = new Instruction(Type.C_COMMAND, dest,
                    comp, jump);

            //System.out.println("comp in class: " + instruction.comp);
            return instruction;
        } 
    }

}
