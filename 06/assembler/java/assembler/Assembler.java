//package assembler;
package assembler;

import parser.*; import instruction.*; import symbol.*;
import java.io.*; 
import java.util.*;

public class Assembler {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("Usage: Assembler *.asm\n");
            return;
        }

        String fileName = args[0].substring(0, args[0].lastIndexOf("."));

        FileInputStream in = null;
        FileOutputStream out = null;
        in = new FileInputStream(args[0]);
        out = new FileOutputStream(fileName + ".hack");

        Symbol symbol = new Symbol();

        BufferedReader pass1 = new BufferedReader(new InputStreamReader(in));
        String line1 = pass1.readLine();

        while (line1 != null) {
            Parser parsedLine = new Parser(line1);
            if (!parsedLine.shortLine.isEmpty()) {
                parsedLine.buildSymbol(symbol);
                parsedLine.pc++;
            }

            line1 = pass1.readLine();
        }

        //reset buffer
        in.getChannel().position(0);

        BufferedReader pass2 = new BufferedReader(new InputStreamReader(in));
        String line2 = pass2.readLine();
            //System.out.println("we are in: " + line2);

        byte[] newLine = System.getProperty("line.separator").getBytes();

        while (line2 != null) {
            Parser parsedLine = new Parser(line2);
            if (!parsedLine.shortLine.isEmpty()) {
                byte[] outputInByte;
                Instruction parsedInstruction = parsedLine.commandType(symbol);
                parsedInstruction.binaryConvert(symbol);
                if (!parsedInstruction.binaryCode.isEmpty()) {
                    System.out.println(parsedInstruction.binaryCode);
                    outputInByte = parsedInstruction.binaryCode.getBytes();
                    out.write(outputInByte);
                    out.write(newLine);
                }

                //if (!parsedLine.commandType().comp.isEmpty()) {
                //    outputInByte = parsedLine.commandType().comp.getBytes();
                //    out.write(outputInByte);
                //    out.write(newLine);
                //}
            }
            line2 = pass2.readLine();
        }


        in.close();
        out.close();
    }
}

