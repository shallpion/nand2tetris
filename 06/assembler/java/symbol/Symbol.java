package symbol;
import type.*;
import java.util.Hashtable;

public class Symbol {

    public static final Hashtable<String, String> destTable =
        new Hashtable<String, String>();
    public static final Hashtable<String, String> compTable =
        new Hashtable<String, String>();
    public static final Hashtable<String, String> jumpTable = 
        new Hashtable<String, String>();
    public static final Hashtable<String, Integer> symTable = 
        new Hashtable<String, Integer>();


    public Symbol() {
        // Initialize dest hashtable
        destTable.put("null",   "000");
        destTable.put("M",      "001");      
        destTable.put("D",      "010");
        destTable.put("MD",     "011");
        destTable.put("A",      "100");
        destTable.put("AM",     "101");
        destTable.put("AD",     "110");
        destTable.put("AMD",    "111");

        // Initialize comp hashtable
        compTable.put("0",      "0101010");
        compTable.put("1",      "0111111");
        compTable.put("-1",     "0111010");
        compTable.put("D",      "0001100");
        compTable.put("A",      "0110000");
        compTable.put("M",      "1110000");
        compTable.put("!D",     "0001101");
        compTable.put("!A",     "0110001");
        compTable.put("!M",     "1110001");
        compTable.put("-D",     "0001101");
        compTable.put("-A",     "0110011");
        compTable.put("-M",     "1110011");
        compTable.put("D+1",    "0011111");
        compTable.put("A+1",    "0110111");
        compTable.put("M+1",    "1110111");
        compTable.put("D-1",    "0001110");
        compTable.put("A-1",    "0110010");
        compTable.put("M-1",    "1110010");
        compTable.put("D+A",    "0000010");
        compTable.put("D+M",    "1000010");
        compTable.put("D-A",    "0010011");
        compTable.put("D-M",    "1010011");
        compTable.put("A-D",    "0000111");
        compTable.put("M-D",    "1000111");
        compTable.put("D&A",    "0000000");
        compTable.put("D&A",    "1000000");
        compTable.put("D|A",    "0010101");
        compTable.put("D|M",    "1010101");

        jumpTable.put("null",   "000");
        jumpTable.put("JGT",    "001");
        jumpTable.put("JEQ",    "010");
        jumpTable.put("JGE",    "011");
        jumpTable.put("JLT",    "100");
        jumpTable.put("JNE",    "101");
        jumpTable.put("JLE",    "110");
        jumpTable.put("JMP",    "111");

        symTable.put("SP",      0);
        symTable.put("LCL",     1);
        symTable.put("ARG",     2);
        symTable.put("THIS",    3);
        symTable.put("THAT",    4);
        symTable.put("R0",	    0);
        symTable.put("R1",	    1);
        symTable.put("R2",	    2);
        symTable.put("R3",	    3);
        symTable.put("R4",	    4);
        symTable.put("R5",	    5);
        symTable.put("R6",	    6);
        symTable.put("R7",	    7);
        symTable.put("R8",	    8);
        symTable.put("R9",	    9);
        symTable.put("R10",	    10);
        symTable.put("R11",	    11);
        symTable.put("R12",	    12);
        symTable.put("R13",	    13);
        symTable.put("R14",	    14);
        symTable.put("R15",	    15);
        symTable.put("SCREEN",  16384);
        symTable.put("KBD",     24576);
    }
}
