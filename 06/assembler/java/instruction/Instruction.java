package instruction;
import type.*;
import symbol.*;
import java.lang.*;
//import java.util.Hashtable;

public class Instruction extends Symbol {
    public int commandType = 0;

    // value is either a symbol or number representing
    // address
    public String value = "";
    public String dest  = "";
    public String comp  = "";
    public String jump  = "";
    //public String symbol= "";

    public String binaryCode = "";
    public Instruction(int type, String string) {
        switch(type) {
            case Type.A_COMMAND:
                commandType = Type.A_COMMAND;
                if (string != null && !string.isEmpty())
                    value = string;
                break;
            case Type.L_COMMAND:
                commandType = Type.L_COMMAND;
                break;
        }
    }

    public Instruction(int type, String dest, String comp, String jump) {
        if (type == Type.C_COMMAND) {
            commandType = Type.C_COMMAND;
            if (!dest.isEmpty())
                this.dest = dest;
            if (!comp.isEmpty())
                this.comp = comp;
            if (!jump.isEmpty()) {
                this.jump = jump;
            }
        }
    }

    public void binaryConvert(Symbol symbol) {


        switch(commandType) {
            case Type.A_COMMAND:
                if (value.isEmpty())
                    return;
                int addr;
                if (value.matches("\\d+")) {
                    addr = Integer.parseInt(value);
                } else {
                    addr = symbol.symTable.get(value);
                }
                String addrBinary = String.format("%15s", Integer.toBinaryString(addr)).replace(' ', '0');
                binaryCode += "0";
                binaryCode += addrBinary;
                break;

            case Type.C_COMMAND:
                binaryCode += "111";

                // comp is never empty
                binaryCode += symbol.compTable.get(comp);

                if (dest.isEmpty()) 
                    binaryCode += symbol.destTable.get("null");
                else 
                    binaryCode += symbol.destTable.get(dest);

                if (jump.isEmpty())
                    binaryCode += symbol.jumpTable.get("null");
                else
                    binaryCode += symbol.jumpTable.get(jump);
                break;
        }
    }
}





















