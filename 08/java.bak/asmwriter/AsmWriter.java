package asmwriter;
import parser.*;
import java.io.*;

public class AsmWriter {
    private static int lbl;     // used for jumping command
    private String fileName;

    public String CR = System.getProperty("line.separator");

    public FileWriter fw = null;
    public AsmWriter(String in) {
        fileName = in.substring(0, in.lastIndexOf("."));
        System.out.println("output:" + fileName + ".asm");
        try {
            fw = new FileWriter(fileName + ".asm");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public String writeArithmetic(Parser parser) {
        switch(parser.arg1) {
            case "add":
                return 
                    "@SP" + CR +
                    "AM=M-1" + CR +     //AM=... instead of A=... 
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "M=D+M" + CR + CR;  //aditional empty line for easy reading
            case "sub":
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +    
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "M=M-D" + CR + CR;
            case "neg":
                return
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=-M" + CR + CR;
            case "and":
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "M=D&M" + CR + CR;
            case "or":
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "M=D|M" + CR + CR;
            case "not":
                return
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=!M" + CR + CR;
            case "eq":
                String s = lbl++ + "";
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "D=M-D" + CR +
                    "@EQ.TRUE" + s + CR +
                    "D;JEQ" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=0" + CR +
                    "@EQ.AFTER" + s + CR +
                    "(EQ.TRUE" + s + ")" + CR +
                    "0;JMP" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR + 
                    "M=-1" + CR +
                    "(EQ.AFTER" + s + ")" + CR + CR;
            case "gt":
                String r = lbl++ + "";
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "D=M-D" + CR +
                    "@GT.TRUE" + r + CR +
                    "D;JGT" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=0" + CR +
                    "@GT.AFTER" + r + CR +
                    "(GT.TRUE" + r + ")" + CR +
                    "0;JMP" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR + 
                    "M=-1" + CR +
                    "(GT.AFTER" + r + ")" + CR + CR;
            case "lt":
                String t = lbl++ + "";
                return
                    "@SP" + CR +
                    "AM=M-1" + CR +
                    "D=M" + CR +
                    "A=A-1" + CR +
                    "D=M-D" + CR +
                    "@LT.TRUE" + t + CR +
                    "D;JLT" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR +
                    "M=0" + CR +
                    "@LT.AFTER" + t + CR +
                    "(LT.TRUE" + t + ")" + CR +
                    "0;JMP" + CR +
                    "@SP" + CR +
                    "A=M-1" + CR + 
                    "M=-1" + CR +
                    "(LT.AFTER" + t + ")" + CR + CR;
            default:
                return "";
        }
    }

    private String PUSH  = 
        "@SP" + CR +
        "A=M" + CR +
        "M=D" + CR +
        "@SP" + CR +
        "M=M+1";


    public String writePush(Parser parser) {
        switch(parser.arg1) {
            case "constant":
                return 
                    "@" + parser.arg2 + CR +
                    "D=A" + CR + 
                    PUSH + CR + CR;
            case "this":        //Push @THIS+arg2 on stack
                return
                    "@THIS" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH + CR + CR;
            case "that":        //Push @THIS+arg2 on stack
                return
                    "@THAT" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH + CR + CR;
            case "static":
                return
                    "@" + fileName + parser.arg2 + CR +
                    "D=M"+ CR +
                    PUSH + CR + CR;
            case "temp":
                return
                    "@R5" + CR +
                    "D=A" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH + CR + CR;
            case "pointer":
                if (parser.arg2.equals("0"))
                    return
                        "@THIS" + CR +
                        "D=M" + CR +
                        PUSH + CR + CR;
                else
                    return
                        "@THAT" + CR +
                        "D=M" + CR +
                        PUSH + CR + CR;
            case "local":        
                return
                    "@LCL" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH + CR + CR;
            case "argument":        
                return
                    "@ARG" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "A=D+A" + CR +
                    "D=M" + CR +
                    PUSH + CR + CR;
            default:
                return "";
        }
    }

    private String POP = 
        "@R13" + CR +
        "M=D" + CR +      //store D's address at temp R13
        "@SP" + CR +
        "AM=M-1" + CR + 
        "D=M" + CR +
        "@R13" + CR +
        "A=M" + CR +
        "M=D" + CR + CR;

    public String writePop(Parser parser) {
        switch(parser.arg1) {
            case "this":        
                return
                    "@THIS" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP + CR +CR;
            case "that":        
                return
                    "@THAT" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP + CR +CR;
            case "static":
                return
                    "@" + fileName + parser.arg2 + CR +
                    "D=A" + CR +
                    POP + CR +CR;
            case "temp":
                return
                    "@R5" + CR +
                    "D=A" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP + CR +CR;
            case "pointer":
                if (parser.arg2.equals("0"))
                    return
                        "@THIS" + CR +
                        "D=A" + CR +
                        POP + CR +CR;
                else
                    return
                        "@THAT" + CR +
                        "D=A" + CR +
                        POP + CR +CR;
            case "local":        
                return
                    "@LCL" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP + CR +CR;
            case "argument":        
                return
                    "@ARG" + CR +
                    "D=M" + CR +
                    "@" + parser.arg2 + CR +
                    "D=D+A" + CR +
                    POP + CR +CR;
            default:
                return "";

        }
    }

    private String callFunc(String arg1, String arg2, String r) {
        return 
            // push return address, which is the end of this call
            "@RET." + r + CR +
            "D=A" + CR +
            PUSH + CR +
            // push *LCL
            "@LCL" + CR +
            "D=M" + CR +
            PUSH + CR +
            // push *ARG
            "@ARG" + CR +
            "D=M" + CR +
            PUSH + CR +
            // push *THIS
            "@ARG" + CR +
            "D=M" + CR +
            // push *THAT
            "@THAT" + CR +
            "D=M" + CR +
            // move ARG to *SP-n-5 position
            "@SP" + CR +
            "D=M" + CR +
            "@" + arg2 + CR +
            "D=D-A" + CR +
            "@5" + CR + 
            "D=D-A" + CR +
            "@ARG" + CR +
            "M=D" + CR +
            // LCL = SP
            "@SP" + CR + 
            "D=M" + CR +
            "@LCL" + CR +
            "M=D" + CR +
            "@" + arg1 + CR +
            "0;JMP" + CR + 
            "(RET." + r + ")" + CR;


    }

    public String writeCall(Parser parser){
        String r = (lbl++) + "";
        return callFunc(parser.arg1, parser.arg2, r) + CR;
    }

    public String writeReturn(Parser parser){
        return
            // RET = *(LCL - 5)
            "@LCL" + CR +
            "D=M" + CR +
            "@5" + CR +
            "D=D-A" + CR +
            "A=D" + CR +
            "D=M" + CR +
            "@R13" + CR +
            "M=D" + CR +

            // *ARG = pop()
            "@SP" + CR +
            "AM=M-1" + CR + // In fact we don't need to change @SP here
            "D=M" + CR +
            "@ARG" + CR +
            "A=M" + CR +
            "M=D" + CR +

            // SP = ARG + 1
            "D=A+1" + CR +
            "@SP" + CR +
            "M=D" + CR +

            // THAT = *(LCL- -)
            "@LCL" + CR +
            "AM=M-1" + CR +
            "D=M" + CR +
            "@THAT" + CR +
            "M=D" + CR +

            // THIS = *(LCL- -)
            "@LCL" + CR +
            "AM=M-1" + CR +
            "D=M" + CR +
            "@THIS" + CR +
            "M=D" + CR +

            // ARG = *(LCL- -)
            "@LCL" + CR +
            "AM=M-1" + CR +
            "D=M" + CR +
            "@LCL" + CR +
            "M=D" + CR +

            // LCL = *(LCL- -)
            "@LCL" + CR +
            "AM=M-1" + CR +
            "D=M" + CR +
            "@LCL" + CR +
            "M=D" + CR +

            "@R13" + CR +
            "A=M" + CR +
            "0;JMP" + CR +CR ;

    }
    public String writeFunc(Parser parser) {
        String s = "";
        s += "(" + parser.arg1 + ")" + CR;

        int k = Integer.parseInt(parser.arg2);

        // push k 0's onto stack
        for (int i = 0; i < k; i++ ) {
            s += "@0" + CR + 
                "D=A" + CR + PUSH + CR;
        }

        return s;
    }
    public String writeInit() {
        String r = lbl++ + "";
//        System.out.println("r is: " + r);
        String s = "";
        s += "@256" + CR +
            "D=A" + CR +
            "@SP" + CR +
            "M=D" + CR;
        s +=  callFunc("Sys.int", "0", r);

        s += "0;JMP" + CR + CR;
        return s;
    }

    public String writeLabel(Parser parser) {
        return
            "(" + parser.funcName + "$" 
            + parser.arg1 + ")" + CR + CR;
    }

    public String writeGoto(Parser parser) {
        return
            "@" + parser.funcName + "$" +
            parser.arg1 + CR + "0;JMP" + CR + CR;
    }

    public String writeIf(Parser parser) {
        return
            "@SP" + CR +
            "AM=M-1" + CR +
            "D=M" + CR +
            "@" + parser.funcName + "$" + parser.arg1 + CR +
            "D;JLT" + CR + CR;

    }
}
